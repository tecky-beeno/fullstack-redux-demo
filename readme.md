# fullstack-redux-demo

This repo demo how to store redux state as normalized tables.
And how to build reducer and APIs around this design.

**Pros:**
- able to reuse the same data model between backend and frontend
- this design is cache-friendly
- simple (single action) logic in redux reducer
- don't need to join tables on the backend

**Cons:**
- need to run "join table logics" on the components' useSelector
