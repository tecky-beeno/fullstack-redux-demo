import Knex from 'knex'

export class AssignmentService {
  constructor(public knex: Knex) {}

  async getAssignmentListByCifu(cifu_id: number) {
    const assignment_list = await this.knex('assignment').where({ cifu_id })

    const order_id_list: number[] = assignment_list.map(row => row.order_id)
    const order_list = await this.knex('order').whereIn('id', order_id_list)

    //     let order_query = this.knex('order')
    //     order_list.forEach(id => {
    //       order_query = order_query.orWhere('id', id)
    //     })

    const client_id_list = order_list.map(row => row.client_id)
    const user_list_p = this.knex('user').whereIn('id', client_id_list)

    const order_image_list_p = this.knex('order_image').whereIn(
      'order_id',
      order_id_list,
    )

    const [user_list, order_image_list] = await Promise.all([
      user_list_p,
      order_image_list_p,
    ])

    return {
      assignment_list,
      order_list,
      user_list,
      order_image_list,
    }
  }
}
