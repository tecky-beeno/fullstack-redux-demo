import { useSelector } from 'react-redux'
import { RootState } from '../redux/state'

const users: Record<number, { id: number; username: string }> = {
  1: { id: 1, username: 'alice' },
  2: { id: 2, username: 'bob' },
}
console.debug(users)

type AssignmentItem = {
  cifu: {
    user_id: number
    username: string
  }
  order: {
    order_id: number
    desc: string
    repair_address: string
    pic_contact: string
    pic_name: string
  }
  client: {
    user_id: number
    username: string
  }
  assignment: {
    assignment_id: number
    repair_date: string
  }
  complete_image_by_cifu_list: string[]
  complete_image_by_client_list: string[]
}

export default function AdminAssignmentList() {
  const assignment_list: AssignmentItem[] = useSelector(
    (state: RootState): AssignmentItem[] => {
      const assignment_list: AssignmentItem[] = []

      Object.values(state.assignment_list).forEach(assignment => {
        const cifu = state.user_list[assignment.cifu_id]
        const order = state.order_list[assignment.order_id]
        if (!cifu || !order) return
        const client = state.user_list[order.client_id]
        if (!client) return

        const complete_image_by_cifu_list: string[] = []
        const complete_image_by_client_list: string[] = []

        Object.values(state.complete_image_list).forEach(image => {
          if (image.assignment_id !== assignment.assignment_id) return
          const user = state.user_list[image.user_id]
          if (!user) return
          switch (user.role) {
            case 'cifu':
              complete_image_by_cifu_list.push(image.image)
              break
            case 'client':
              complete_image_by_client_list.push(image.image)
              break
          }
        })

        assignment_list.push({
          assignment,
          cifu,
          client,
          order,
          complete_image_by_cifu_list,
          complete_image_by_client_list,
        })
        // assignment_list.push({
        //   assignment: {
        //     assignment_id: assignment.assignment_id,
        //     repair_date: assignment.repair_date,
        //   },
        //   cifu: {
        //     user_id: cifu.user_id,
        //     username: cifu.username,
        //   },
        //   order: {
        //     order_id: order.order_id,
        //     desc: order.desc,
        //     repair_address: order.repair_address,
        //   },
        //   client: {
        //     user_id: client.user_id,
        //     username: client.username,
        //   },
        // })
      })

      return assignment_list
    },
  )
  return (
    <div className="list">
      {assignment_list.map(({ order, client, cifu, assignment }) => (
        <div className="card">
          <div className="order-title">
            <a href={`/order/${order.order_id}`}>
              <span className="order-id">#{order.order_id}</span>
            </a>
            <a href={`/client/${client.user_id}`}>
              <span className="client-name">{client.username}</span>
            </a>
          </div>
          <div className="order-preview">
            <div className="order-address">{order.repair_address}</div>
            <p>{order.desc}</p>
            <p>
              PIC: {order.pic_name} ({order.pic_contact})
            </p>
          </div>
          <div className="order-assignment">
            <a href={`/assignment/${assignment.assignment_id}`}>
              #{assignment.assignment_id}
            </a>
            <time>{assignment.repair_date}</time>
            <a href={`/cifu/${cifu.user_id}`}>
              <span className="cifu-id">#{cifu.user_id}</span>
              <span className="cifu-name">{cifu.username}</span>
            </a>
          </div>
        </div>
      ))}
    </div>
  )
}
