import { useSelector } from 'react-redux'
import { RootState } from '../redux/state'
import { toUrl } from './image'

const users: Record<number, { id: number; username: string }> = {
  1: { id: 1, username: 'alice' },
  2: { id: 2, username: 'bob' },
}
console.debug(users)

type AssignmentItem = {
  order: {
    order_id: number
    desc: string
    repair_address: string
    pic_contact: string
    pic_name: string
  }
  client: {
    user_id: number
    username: string
  }
  assignment: {
    assignment_id: number
    repair_date: string
  }
  order_image_list: string[]
}

export default function AdminAssignmentList() {
  const assignment_list: AssignmentItem[] = useSelector(
    (state: RootState): AssignmentItem[] => {
      const assignment_list: AssignmentItem[] = []

      Object.values(state.assignment_list).forEach(assignment => {
        const order = state.order_list[assignment.order_id]
        if (!order) return
        const client = state.user_list[order.client_id]
        if (!client) return
        const order_image_list = Object.values(state.order_image_list)
          .filter(image => image.order_id === order.order_id)
          .map(image => toUrl(image.image))
        assignment_list.push({
          assignment,
          order,
          client,
          order_image_list,
        })
      })
      return assignment_list
    },
  )
  return (
    <div className="list">
      {assignment_list.map(
        ({ order, assignment, client, order_image_list }) => (
          <div className="card">
            <div className="order-title">
              <a href={`/order/${order.order_id}`}>
                <span className="order-id">#{order.order_id}</span>
              </a>
              <a href={`/client/${client.user_id}`}>
                <span className="client-name">{client.username}</span>
              </a>
            </div>
            <div className="order-preview">
              <div className="order-address">{order.repair_address}</div>
              <p>{order.desc}</p>
              <p>
                PIC: {order.pic_name} ({order.pic_contact})
              </p>
            </div>
            <div className="order-assignment">
              <a href={`/assignment/${assignment.assignment_id}`}>
                #{assignment.assignment_id}
              </a>
              <time>{assignment.repair_date}</time>
            </div>
            <div className="order-image-list">
              {order_image_list.map(image => (
                <div className="order-image">
                  <img src={image} />
                </div>
              ))}
            </div>
          </div>
        ),
      )}
    </div>
  )
}
