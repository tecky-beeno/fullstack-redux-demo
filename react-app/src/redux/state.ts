export type RootState = {
  auth: {
    token: string
    user_id: number
    role: 'admin' | 'cifu' | 'client'
  } | null
  order_list: Record<
    number,
    {
      order_id: number
      client_id: number
      desc: string
      repair_address: string
      pic_contact: string
      pic_name: string
    }
  >
  order_image_list: Record<
    number,
    {
      order_id: number
      image: string
    }
  >
  assignment_list: Record<
    number,
    {
      assignment_id: number
      order_id: number
      cifu_id: number
      remark: string
      repair_date: string
    }
  >
  complete_image_list: Record<
    number,
    {
      assignment_id: number
      image: string
      user_id: number
    }
  >
  //   complete_image_by_cifu_list: Record<
  //     number,
  //     {
  //       assignment_id: number
  //       image: string
  //     }
  //   >
  //   complete_image_by_client_list: Record<
  //     number,
  //     {
  //       assignment_id: number
  //       image: string
  //     }
  //   >
  user_list: Record<
    number,
    {
      user_id: number
      username: string
      role: 'admin' | 'cifu' | 'client'
    }
  >
}
