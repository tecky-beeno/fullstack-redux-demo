import { RootState } from './state'

export function setState(partialState: Partial<RootState>) {
  return {
    type: '@@APP/setState' as const,
    partialState,
  }
}

export type RootAction = ReturnType<typeof setState>
