import { ThunkDispatch } from 'redux-thunk'
import { RootState } from './state'
import { RootAction, setState } from './action'

export type RootDispatch = ThunkDispatch<RootState, null, RootAction>

const server = 'http://server'
export function getAssignmentList() {
  return async (dispatch: RootDispatch) => {
    const res = await fetch(server + '/assignment/all')
    const json = await res.json()
    if (json.error) {
      // TODO dispatch error
      return
    }
    const partialState = json.data
    dispatch(setState(partialState))
  }
}
