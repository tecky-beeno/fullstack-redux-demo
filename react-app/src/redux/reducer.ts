import { RootState } from './state'
import { RootAction } from './action'

const initialState: RootState = {
  auth: null,
  order_list: {},
  order_image_list: {},
  assignment_list: {},
  complete_image_list: {},
  user_list: {},
}

export function rootReducer(
  state: RootState = initialState,
  action: RootAction,
): RootState {
  switch (action.type) {
    case '@@APP/setState': {
      const { partialState } = action
      if (partialState.auth) {
        state = { ...state, auth: partialState.auth }
      }
      if (partialState.order_list) {
        state = {
          ...state,
          order_list: {
            ...state.order_list,
            ...partialState.order_list,
          },
        }
      }
      type Key = keyof Omit<RootState, 'auth'>
      const keys: Key[] = [
        'order_image_list',
        'assignment_list',
        'complete_image_list',
        'user_list',
      ]
      keys.forEach(key => {
        if (!partialState[key]) return
        state = {
          ...state,
          [key]: {
            ...state[key],
            ...partialState[key],
          },
        }
      })
      return state
    }
    default:
      return state
  }
}
